import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class WishComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-wish div table .btn-danger'));
  title = element.all(by.css('jhi-wish div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class WishUpdatePage {
  pageTitle = element(by.id('jhi-wish-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  descriptionInput = element(by.id('field_description'));
  unitPriceInput = element(by.id('field_unitPrice'));
  quantityInput = element(by.id('field_quantity'));
  unitInput = element(by.id('field_unit'));
  modelInput = element(by.id('field_model'));
  brandInput = element(by.id('field_brand'));
  wishListSelect = element(by.id('field_wishList'));
  userSelect = element(by.id('field_user'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDescriptionInput(description) {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput() {
    return await this.descriptionInput.getAttribute('value');
  }

  async setUnitPriceInput(unitPrice) {
    await this.unitPriceInput.sendKeys(unitPrice);
  }

  async getUnitPriceInput() {
    return await this.unitPriceInput.getAttribute('value');
  }

  async setQuantityInput(quantity) {
    await this.quantityInput.sendKeys(quantity);
  }

  async getQuantityInput() {
    return await this.quantityInput.getAttribute('value');
  }

  async setUnitInput(unit) {
    await this.unitInput.sendKeys(unit);
  }

  async getUnitInput() {
    return await this.unitInput.getAttribute('value');
  }

  async setModelInput(model) {
    await this.modelInput.sendKeys(model);
  }

  async getModelInput() {
    return await this.modelInput.getAttribute('value');
  }

  async setBrandInput(brand) {
    await this.brandInput.sendKeys(brand);
  }

  async getBrandInput() {
    return await this.brandInput.getAttribute('value');
  }

  async wishListSelectLastOption(timeout?: number) {
    await this.wishListSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async wishListSelectOption(option) {
    await this.wishListSelect.sendKeys(option);
  }

  getWishListSelect(): ElementFinder {
    return this.wishListSelect;
  }

  async getWishListSelectedOption() {
    return await this.wishListSelect.element(by.css('option:checked')).getText();
  }

  async userSelectLastOption(timeout?: number) {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option) {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption() {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class WishDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-wish-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-wish'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
