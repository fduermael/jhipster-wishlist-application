/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { WishComponentsPage, WishDeleteDialog, WishUpdatePage } from './wish.page-object';

const expect = chai.expect;

describe('Wish e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let wishUpdatePage: WishUpdatePage;
  let wishComponentsPage: WishComponentsPage;
  let wishDeleteDialog: WishDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Wishes', async () => {
    await navBarPage.goToEntity('wish');
    wishComponentsPage = new WishComponentsPage();
    await browser.wait(ec.visibilityOf(wishComponentsPage.title), 5000);
    expect(await wishComponentsPage.getTitle()).to.eq('wishListApplicationApp.wish.home.title');
  });

  it('should load create Wish page', async () => {
    await wishComponentsPage.clickOnCreateButton();
    wishUpdatePage = new WishUpdatePage();
    expect(await wishUpdatePage.getPageTitle()).to.eq('wishListApplicationApp.wish.home.createOrEditLabel');
    await wishUpdatePage.cancel();
  });

  it('should create and save Wishes', async () => {
    const nbButtonsBeforeCreate = await wishComponentsPage.countDeleteButtons();

    await wishComponentsPage.clickOnCreateButton();
    await promise.all([
      wishUpdatePage.setDescriptionInput('description'),
      wishUpdatePage.setUnitPriceInput('5'),
      wishUpdatePage.setQuantityInput('5'),
      wishUpdatePage.setUnitInput('unit'),
      wishUpdatePage.setModelInput('model'),
      wishUpdatePage.setBrandInput('brand'),
      wishUpdatePage.wishListSelectLastOption(),
      wishUpdatePage.userSelectLastOption()
    ]);
    expect(await wishUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await wishUpdatePage.getUnitPriceInput()).to.eq('5', 'Expected unitPrice value to be equals to 5');
    expect(await wishUpdatePage.getQuantityInput()).to.eq('5', 'Expected quantity value to be equals to 5');
    expect(await wishUpdatePage.getUnitInput()).to.eq('unit', 'Expected Unit value to be equals to unit');
    expect(await wishUpdatePage.getModelInput()).to.eq('model', 'Expected Model value to be equals to model');
    expect(await wishUpdatePage.getBrandInput()).to.eq('brand', 'Expected Brand value to be equals to brand');
    await wishUpdatePage.save();
    expect(await wishUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await wishComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Wish', async () => {
    const nbButtonsBeforeDelete = await wishComponentsPage.countDeleteButtons();
    await wishComponentsPage.clickOnLastDeleteButton();

    wishDeleteDialog = new WishDeleteDialog();
    expect(await wishDeleteDialog.getDialogTitle()).to.eq('wishListApplicationApp.wish.delete.question');
    await wishDeleteDialog.clickOnConfirmButton();

    expect(await wishComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
