import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class AnswerComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-answer div table .btn-danger'));
  title = element.all(by.css('jhi-answer div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class AnswerUpdatePage {
  pageTitle = element(by.id('jhi-answer-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  quantityInput = element(by.id('field_quantity'));
  unitInput = element(by.id('field_unit'));
  modelInput = element(by.id('field_model'));
  brandInput = element(by.id('field_brand'));
  answerListSelect = element(by.id('field_answerList'));
  wishSelect = element(by.id('field_wish'));
  userSelect = element(by.id('field_user'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setQuantityInput(quantity) {
    await this.quantityInput.sendKeys(quantity);
  }

  async getQuantityInput() {
    return await this.quantityInput.getAttribute('value');
  }

  async setUnitInput(unit) {
    await this.unitInput.sendKeys(unit);
  }

  async getUnitInput() {
    return await this.unitInput.getAttribute('value');
  }

  async setModelInput(model) {
    await this.modelInput.sendKeys(model);
  }

  async getModelInput() {
    return await this.modelInput.getAttribute('value');
  }

  async setBrandInput(brand) {
    await this.brandInput.sendKeys(brand);
  }

  async getBrandInput() {
    return await this.brandInput.getAttribute('value');
  }

  async answerListSelectLastOption(timeout?: number) {
    await this.answerListSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async answerListSelectOption(option) {
    await this.answerListSelect.sendKeys(option);
  }

  getAnswerListSelect(): ElementFinder {
    return this.answerListSelect;
  }

  async getAnswerListSelectedOption() {
    return await this.answerListSelect.element(by.css('option:checked')).getText();
  }

  async wishSelectLastOption(timeout?: number) {
    await this.wishSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async wishSelectOption(option) {
    await this.wishSelect.sendKeys(option);
  }

  getWishSelect(): ElementFinder {
    return this.wishSelect;
  }

  async getWishSelectedOption() {
    return await this.wishSelect.element(by.css('option:checked')).getText();
  }

  async userSelectLastOption(timeout?: number) {
    await this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async userSelectOption(option) {
    await this.userSelect.sendKeys(option);
  }

  getUserSelect(): ElementFinder {
    return this.userSelect;
  }

  async getUserSelectedOption() {
    return await this.userSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class AnswerDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-answer-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-answer'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
