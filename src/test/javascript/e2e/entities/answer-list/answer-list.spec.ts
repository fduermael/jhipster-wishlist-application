/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { AnswerListComponentsPage, AnswerListDeleteDialog, AnswerListUpdatePage } from './answer-list.page-object';

const expect = chai.expect;

describe('AnswerList e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let answerListUpdatePage: AnswerListUpdatePage;
  let answerListComponentsPage: AnswerListComponentsPage;
  let answerListDeleteDialog: AnswerListDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load AnswerLists', async () => {
    await navBarPage.goToEntity('answer-list');
    answerListComponentsPage = new AnswerListComponentsPage();
    await browser.wait(ec.visibilityOf(answerListComponentsPage.title), 5000);
    expect(await answerListComponentsPage.getTitle()).to.eq('wishListApplicationApp.answerList.home.title');
  });

  it('should load create AnswerList page', async () => {
    await answerListComponentsPage.clickOnCreateButton();
    answerListUpdatePage = new AnswerListUpdatePage();
    expect(await answerListUpdatePage.getPageTitle()).to.eq('wishListApplicationApp.answerList.home.createOrEditLabel');
    await answerListUpdatePage.cancel();
  });

  it('should create and save AnswerLists', async () => {
    const nbButtonsBeforeCreate = await answerListComponentsPage.countDeleteButtons();

    await answerListComponentsPage.clickOnCreateButton();
    await promise.all([]);
    await answerListUpdatePage.save();
    expect(await answerListUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await answerListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last AnswerList', async () => {
    const nbButtonsBeforeDelete = await answerListComponentsPage.countDeleteButtons();
    await answerListComponentsPage.clickOnLastDeleteButton();

    answerListDeleteDialog = new AnswerListDeleteDialog();
    expect(await answerListDeleteDialog.getDialogTitle()).to.eq('wishListApplicationApp.answerList.delete.question');
    await answerListDeleteDialog.clickOnConfirmButton();

    expect(await answerListComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
